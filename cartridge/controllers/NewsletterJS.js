'use strict';
/* API Includes */
var URLUtils = require('dw/web/URLUtils');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
require('dw/object');

/* Script Modules */
var guard = require('sitegenesis_storefront_controllers/cartridge/scripts/guard');
var app = require('sitegenesis_storefront_controllers/cartridge/scripts/app');
function start(){
app.getForm('profile').clear();
/* use the app module to clear “newsletter” form */
app.getView({
	Action: 'subscribe' ,
	ContinueURL: URLUtils.https('NewsletterJS-HandleForm')
}).render('newsletter/newslettersignup.isml');
}
function handleForm() {

 Address = app.getModel('Address');
var newsletterForm = app.getForm('newsletter');
newsletterForm.handleAction({
	subscribe: function(){
		try {
			Transaction.wrap(function() {
				var co:CustomObject = CustomObjectMgr.createCustomObject('Newsletter', session.forms.newsletter.email.value);
				newsletterForm.copyTo(co);
				});
		} catch (e if e instanceof SystemError) {
			app.getView().render('newsletter/newslettererror.isml');
			return;
		} 
		

		app.getView({
			CurrentForms: session.forms
		}).render('newsletter/newslettersuccess.isml');
	}
});
return;
}
exports.Start = guard.ensure(['get'], start);
exports.HandleForm = guard.ensure([], handleForm);