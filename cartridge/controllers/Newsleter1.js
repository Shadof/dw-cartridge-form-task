'use strict';
/** @module controllers/Newsleter1 */
var ISML = require('dw/template/ISML');
var guard = require('sitegenesis_storefront_controllers/cartridge/scripts/guard');
function start() {
var newsletterForm = session.forms.newsletter;
newsletterForm.clearFormElement();
ISML.renderTemplate('newsletter/newslettersignup.isml', {
	ContinueURL : dw.web.URLUtils.url('Newsleter1-HandleForm'),
	CurrentForms :session.forms
	});
}
function handleForm() {
var TriggeredAction = request.triggeredFormAction; 
response.getWriter().println('Hello World from pipeline controllers!'+TriggeredAction);
if (TriggeredAction != null) {
if (TriggeredAction.formId == 'subscribe') {
var newsletterForm = session.forms.newsletter;
var Transaction = require('dw/system/Transaction');
Transaction.wrap(function() {
		newsletterForm.accept();
		});
response.getWriter().println('Hello World from pipeline controllers!'+newsletterForm.fname.value);
	ISML.renderTemplate('newsletter/newslettersuccess', {
		CurrentForms : session.forms
		});

return;
}
}
}
exports.Start = guard.ensure([], start);
exports.HandleForm = guard.ensure([],handleForm);


