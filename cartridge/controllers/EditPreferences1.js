'use strict';
/** @module controllers/EditPreferences1 */
var ISML = require('dw/template/ISML');
var guard = require('sitegenesis_storefront_controllers/cartridge/scripts/guard');
require('dw/system');
require('dw/object');
require('dw/web');
var preferencesForm:Form = session.forms.preferences;
function start() {
preferencesForm.clearFormElement();
/* can you clear the form object preferencesForm (check API for help)*/
preferencesForm.copyFrom('customer.profile');
ISML.renderTemplate('editpreferences.isml',{
	ContinueURL : dw.web.URLUtils.url('EditPreferences1-HandleForm'),
	CurrentForms: session.forms
	});
}
/* display a form named editpreferences.isml with the ‘CurrentForms’ as form objects on pdict 
 * and HandleForm method (below) to handle the form submission */
/** * The form handler. */
function handleForm() {
var TriggeredAction = request.triggeredFormAction;
response.getWriter().println('Triggered action is '+TriggeredAction.formId);
var preferencesForm:dw.web.Form = session.forms.preferences;
response.getWriter().println('Electronics preference is !'+preferencesForm.interestElectronics.value);
// preferencesForm.accept();
/* Start an implicit transaction and wrap the following line of code in implicit transaction */
var Transaction = require('dw/system/Transaction');
Transaction.wrap(function() {
	preferencesForm.accept();
	});

/* Can you check from the API what the above method does. Can you explain it to your instructor */
response.redirect(dw.web.URLUtils.https('Account-Show'));
return;
}

/*
* Web exposed methods
*/
/** @see module:controllers/SendToFriend~Start */
exports.Start = guard.ensure(['get', 'https', 'loggedIn'], start);
exports.HandleForm = guard.ensure(['post'], handleForm);