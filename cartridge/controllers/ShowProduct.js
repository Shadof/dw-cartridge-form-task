'use strict';
/** @module controllers/ShowProduct */
var ISML = require('dw/template/ISML');
/* Script Modules */
var guard = require('sitegenesis_storefront_controllers/cartridge/scripts/guard');
function start() {
var HttpMap = request.httpParameterMap;
var productResult = null;
if (request.httpParameterMap.pid.toString()) {
productResult = getProduct(request.httpParameterMap.pid);
}
/* direct it to an ISML named ‘productfound’ and load Product on pdict as ‘myProduct */
ISML.renderTemplate('product',{myProduct:productResult.Product})
/*Hint: Product can be extracted as productResult.Product */
}
function getProduct(pid) {
var GetProductResult = new dw.system.Pipelet('GetProduct').execute({
ProductID : pid.stringValue
});
if (GetProductResult.result == PIPELET_ERROR) {
return {
error : true
};
}
var Product = GetProductResult.Product;
// var Product=dw.catalog.ProductMgr.getProduct(pid.value);
// response.getWriter().println(Product.name);
return {
Product :GetProductResult.Product
};
}
/*
* Web exposed methods
*/
exports.Start = guard.ensure(['http', 'get'], start);