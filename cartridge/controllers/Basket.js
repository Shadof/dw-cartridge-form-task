/**
*
* @module controllers/Basket
*/
var ISML = require('dw/template/ISML');
var guard = require('sitegenesis_storefront_controllers/cartridge/scripts/guard');
//var basket = require('dw/Object/Basket');
//var app = require('~/cartridge/scripts/app');
start = function(){
//	app.getModel('Cart').goc();
var basketResult = new dw.system.Pipelet('GetBasket').execute();
var basket:Basket=basketResult.Basket;
/* direct to an isml named ‘showBasket.isml’ and send basket as myBasket on the pdict */
ISML.renderTemplate('showBasket.isml',{myBasket : basket}
)};
exports.Start= guard.ensure(['get'], start);