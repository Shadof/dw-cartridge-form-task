var ajax =  require('../ajax')
util = require('../util');
function CallService()
    {
	jQuery("#name , #direction").on('change',function(){
		 ajax.load({
	            url: Urls.jad,
	            data: {'sort': jQuery("#direction").val()+jQuery("#name").val(), "ajax":"true","start":window.Start>0 ? window.Start : 0,"size":window.Size>0 ? window.Size : 0},
	            callback: function (response) {
	            	jQuery('#mainTable').html(response);
	            }
	        });
		 return false;
	});
}

function OnError(request, status, error)
{
    alert('Error');
}
function checkBox()
{
	jQuery(".m").each(function(index){
	jQuery(this).on('click',function(){
		jQuery("."+(index+1)).toggle();
		});	
	});
}
function ajaxFormSubmit(){
	jQuery("#jadform").submit(function(e) {
		ajax.load({
	           type: "POST",
	           url: Urls.continueUrl,
data: {"dwfrm_jadform_fname":jQuery("#dwfrm_jadform_lname").val(),
	"dwfrm_jadform_fname":jQuery("#dwfrm_jadform_lname").val(),
	"dwfrm_jadform_lname":jQuery("#dwfrm_jadform_lname").val(),
	"dwfrm_jadform_address":jQuery("#dwfrm_jadform_address").val(),
	"dwfrm_jadform_email":jQuery("#dwfrm_jadform_email").val(),
	"dwfrm_jadform_city":jQuery("#dwfrm_jadform_city").val(),
	"dwfrm_jadform_country":jQuery("#dwfrm_jadform_country").val(),
	"dwfrm_jadform_state":jQuery("#dwfrm_jadform_state").val(),
	"dwfrm_jadform_phone":jQuery("#dwfrm_jadform_phone").val(),
	"dwfrm_jadform_check":jQuery("#dwfrm_jadform_check").val(),
	"dwfrm_jadform_subscribe":"Submit"},
	           callback: function (response)
	           {
	               jQuery("#ajaxWrapper").html(jQuery(response).find("#ajaxWrapper")); // show response
	               ajaxFormSubmit();//call function to add same function on submit if user needs to resubmit form
	           },
	           error: function (data){
	        	   alert(data);
	           }
	         });
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
}

exports.init = function () {
	CallService();
	checkBox();
	ajaxFormSubmit();
};