/**
* A hello world controller. This file is in cartridge/controllers folder
*
* @module controllers/HelloWorld This is used only to generate JSDoc
*/
var guard = require('sitegenesis_storefront_controllers/cartridge/scripts/guard');
var ISML = require('dw/template/ISML');

exports.Start = function(){
	response.setContentType('text/html');

	var Message= request.httpParameterMap.param.stringValue;
	
	ISML.renderTemplate(
			'helloController',{Message: Message}
	);
};

exports.Start.public=true;